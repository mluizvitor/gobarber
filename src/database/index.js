import Sequelize from 'sequelize';
import mongoose from 'mongoose';
import databaseConfig from '../config/database';

// importa todos os models da aplicação
import User from '../app/models/User';
import File from '../app/models/File';
import Appointment from '../app/models/Appointment';

// array 'models' serve para fazer mapeamento de models
const models = [User, File, Appointment];

// inicia a classe Database
class Database {
  constructor() {
    this.init();
    this.mongo();
  }

  /**
   * método init para criar a conexão
   */
  init() {
    // cria a conexão como um objeto de Sequelize, pasando a configuração como parâmetro
    this.connection = new Sequelize(databaseConfig);

    // mapea cada valor de 'models' e para cada model inicia o método 'init' com a conexão do banco
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }

  /**
   * método para conectar ao mongoDB
   */
  mongo() {
    this.mongoConnection = mongoose.connect(
      'mongodb://localhost:27017/gobarber',
      {
        useNewUrlParser: true,
        useFindAndModify: true,
        useUnifiedTopology: true,
      }
    );
  }
} // fim da classe Database

export default new Database();
