import * as Yup from 'yup';

import User from '../models/User';
import File from '../models/File';

// início de UserController
class UserController {
  // lista todos os usuários
  async index(req, res) {
    const users = await User.findAll({
      attributes: ['id', 'name', 'email', 'provider', 'avatar_id'],
      include: [
        {
          model: File,
          as: 'avatar',
          attributes: ['name', 'path', 'url'],
        },
      ],
    });

    return res.json(users);
  } // fim da listagem de usuários

  // cria um usuário
  async store(req, res) {
    // validação dos campos no corpo da requisição
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string()
        .required()
        .min(6),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validação falhou' });
    } // fim da validação

    // verifica se usuário existe
    const userExists = await User.findOne({
      where: {
        email: req.body.email,
      },
    });

    if (userExists) {
      return res.status(400).json({ error: 'Email já cadastrado' });
    } // fim da verificação de existência de usuário

    const { id, name, email, provider } = await User.create(req.body);

    return res.json({ id, name, email, provider });
  } // fim da criação de usuário

  // edita o usuário logado no sistema
  async update(req, res) {
    // validação dos campos no corpo da requisição
    const schema = Yup.object().shape({
      name: Yup.string(),
      email: Yup.string().email(),
      oldPassword: Yup.string().min(6),
      password: Yup.string()
        .min(6)
        .when('oldPassword', (oldPassword, field) =>
          oldPassword ? field.required() : field
        ),
      confirmPassword: Yup.string().when('password', (password, field) =>
        password ? field.required().oneOf([Yup.ref('password')]) : field
      ),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validação falhou' });
    } // fim da validação
    const user = await User.findByPk(req.userId);

    const { email, oldPassword } = req.body;

    // verifica se email inserido é diferente do atual
    if (email && email !== user.email) {
      const userExists = await User.findOne({ where: { email } });

      // verifica se o email inserido já está em uso
      if (userExists) {
        return res.status(400).json({ error: 'Email já cadastrado' });
      }
    } // fim da verificação de email

    // verifica se a senha antiga foi inserida e se é igual ou não à senha na base de dados
    if (oldPassword && !(await user.checkPassword(oldPassword))) {
      return res.status(401).json({ error: 'Senha incorreta' });
    }

    const { id, name, provider } = await user.update(req.body);

    return res.json({
      id,
      name,
      email,
      provider,
    });
  } // fim da edição de usuário
} // fim de UserController
export default new UserController();
