// este arquivo contem a estrutura da aplicação

// importa o express e path
import express from 'express';
import path from 'path';
// importa arquivo que contem as rotas da aplicação
import routes from './routes';
// importa as configurações de conexão do banco
import './database';

// cria a classe principal da aplicação
class App {
  // constructor sempre é chamado quando a aplicação rodar, então nele ficam
  // configurados algumas facilidades
  constructor() {
    this.server = express();

    this.middlewares();
    this.routes();
  }

  // método que contem os middlewares
  middlewares() {
    this.server.use(express.json());
    this.server.use(
      '/files',
      express.static(path.resolve(__dirname, '..', 'tmp', 'uploads'))
    );
  }

  // método que chama as rotas
  routes() {
    this.server.use(routes);
  }
}

// exporta o conteúdo de 'server' da classe 'App'.
export default new App().server;
