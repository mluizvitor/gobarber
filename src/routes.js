import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import UserController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import FileController from './app/controllers/FileController';
import ProviderController from './app/controllers/ProviderController';

import authMiddleware from './app/middlewares/auth';
import AppointmentController from './app/controllers/AppointmentController';
import ScheduleController from './app/controllers/ScheduleController';

const routes = new Router();
const upload = multer(multerConfig);

// Rotas sem necessidade de proteção
routes.post('/users', UserController.store);
routes.post('/session', SessionController.store);

// middleware de autenticação
routes.use(authMiddleware);

// Rotas de Providers
routes.get('/providers', ProviderController.index);

// Rotas de Usuários
routes.get('/users', UserController.index);
routes.put('/users', UserController.update);

// Upload de imagem
routes.post('/files', upload.single('file'), FileController.store);

// Rota de Appointments
routes.get('/appointments', AppointmentController.index);
routes.post('/appointments', AppointmentController.store);

// Rota de Schedule
routes.get('/schedule', ScheduleController.index);

export default routes;
